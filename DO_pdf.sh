#!/bin/sh -e

# initial latex
pdflatex branty.tex

# make bibliography
bibtex branty

# make glossary
bibtex branty.gls

# make list of symbols
bibtex branty.losa

# make index
makeindex -s gatech-thesis-index.ist branty.idx

# twice more
pdflatex branty.tex
pdflatex branty.tex

# last of all, create the thumbnails
# Acrobat Reader 5 generates these on-the-fly when
# the document is loaded, but that's slow.  This
# process makes .png previews of each page, and
# puts them into the endproduct.  That way, the 
# file has thumbnails even in Acrobat Reader 4, 
# and it opens much faster in Acrobat Reader 5.
#
# First make the thumbnails
thumbpdf.pl branty.pdf
# Then add them to the doc
pdflatex branty.tex

