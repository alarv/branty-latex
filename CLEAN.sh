#!/bin/sh
rm branty.lot
rm branty.lof
rm branty.toc
rm branty.gls.*
rm branty.losa.*
rm branty.idx
rm branty.ind
rm branty.ilg
rm branty.lol
rm branty.out
rm branty.bbl
rm branty.blg
rm branty.brf
rm branty.tpt
rm *.aux
rm *.log
rm *.bak
rm *~

