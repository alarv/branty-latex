\contentsline {chapter}{\MakeUppercase {Acknowledgements}}{iii}{section*.2}
\setcounter {tocdepth}{2}
\setcounter {tocdepth}{2}
\contentsline {chapter}{\MakeUppercase {List of Tables} }{viii}{section*.6}
\setcounter {tocdepth}{2}
\contentsline {chapter}{\MakeUppercase {List of Figures} }{ix}{section*.8}
\setcounter {tocdepth}{2}
\contentsline {chapter}{\MakeUppercase {List of Symbols or Abbreviations} }{xi}{section*.10}
\setcounter {tocdepth}{2}
\contentsline {chapter}{\MakeUppercase {Glossary} }{xii}{section*.12}
\setcounter {tocdepth}{2}
\contentsline {chapter}{\MakeUppercase {List of Listings} }{xiii}{section*.14}
\setcounter {tocdepth}{2}
\contentsline {chapter}{\MakeUppercase {Abstract}}{xiii}{section*.16}
\setcounter {tocdepth}{2}
\setcounter {tocdepth}{2}
\contentsline {chapter}{\numberline {1}\MakeUppercase {Introduction}}{1}{chapter.18}
\contentsline {section}{\numberline {1.1}Problem definition}{1}{section.19}
\contentsline {section}{\numberline {1.2}Motivation of this thesis}{1}{section.20}
\contentsline {section}{\numberline {1.3}Objective of this thesis}{2}{section.21}
\contentsline {section}{\numberline {1.4}Structure of this thesis}{2}{section.24}
\contentsline {section}{\numberline {1.5}Contribution of this thesis}{2}{section.25}
\orig@contentsline {part}{CHAPTERS}{}
\setcounter {tocdepth}{2}
\contentsline {chapter}{\numberline {2}\MakeUppercase {Theoretical Framework}}{3}{chapter.26}
\contentsline {section}{\numberline {2.1}Twitter Service}{3}{section.27}
\contentsline {subsection}{\numberline {2.1.1}History}{4}{subsection.28}
\contentsline {subsection}{\numberline {2.1.2}Twitter Components}{6}{subsection.29}
\contentsline {section}{\numberline {2.2}Machine Learning}{7}{section.30}
\contentsline {subsection}{\numberline {2.2.1}Sentiment Analysis}{9}{subsection.37}
\contentsline {subsection}{\numberline {2.2.2}Preprocessing}{11}{subsection.38}
\contentsline {section}{\numberline {2.3}Multi Criteria Decision Analysis}{13}{section.43}
\contentsline {subsection}{\numberline {2.3.1}Introduction to Multiple Criteria Decision Analysis - \losa@gloss@i {MCDA}}{14}{subsection.44}
\contentsline {subsection}{\numberline {2.3.2}TOPSIS - Description of used MCDA method}{17}{subsection.54}
\contentsline {subsubsection}{\numberline {2.3.2.1}TOPSIS Algorithm}{18}{subsubsection.59}
\contentsline {section}{\numberline {2.4}Promotional strategies}{20}{section.75}
\setcounter {tocdepth}{2}
\contentsline {chapter}{\numberline {3}\MakeUppercase {Tools}}{21}{chapter.76}
\contentsline {section}{\numberline {3.1}Twitter API}{21}{section.77}
\contentsline {subsection}{\numberline {3.1.1}REST APIs}{21}{subsection.78}
\contentsline {subsection}{\numberline {3.1.2}Streaming API}{22}{subsection.79}
\contentsline {section}{\numberline {3.2}Twitter4J Library}{22}{section.80}
\contentsline {section}{\numberline {3.3}Waikato Environment for Knowdlege Analysis - \losa@gloss@i {WEKA}}{22}{section.81}
\contentsline {section}{\numberline {3.4}SentiWordNet}{23}{section.82}
\contentsline {section}{\numberline {3.5}CoreNLP}{23}{section.84}
\contentsline {section}{\numberline {3.6}Mongo DataBase}{24}{section.86}
\contentsline {section}{\numberline {3.7}Multi-thread Programming}{26}{section.98}
\contentsline {section}{\numberline {3.8}Klout Service}{27}{section.100}
\contentsline {subsection}{\numberline {3.8.1}Overview}{27}{subsection.101}
\contentsline {subsection}{\numberline {3.8.2}Klout API}{28}{subsection.102}
\setcounter {tocdepth}{2}
\contentsline {chapter}{\numberline {4}\MakeUppercase {Implementation}}{30}{chapter.104}
\contentsline {section}{\numberline {4.1}Building a social based brand ranking framework}{30}{section.105}
\contentsline {subsection}{\numberline {4.1.1}Defining a brand page}{31}{subsection.106}
\contentsline {subsection}{\numberline {4.1.2}Defining Features}{31}{subsection.108}
\contentsline {subsection}{\numberline {4.1.3}Architecture}{34}{subsection.141}
\contentsline {subsubsection}{\numberline {4.1.3.1}Application level}{34}{subsubsection.144}
\contentsline {subsubsection}{\numberline {4.1.3.2}Presentation level}{36}{subsubsection.145}
\contentsline {subsection}{\numberline {4.1.4}Database Model}{36}{subsection.147}
\contentsline {subsubsection}{\numberline {4.1.4.1}Brand Collection}{36}{subsubsection.148}
\contentsline {subsubsection}{\numberline {4.1.4.2}Features Collection}{40}{subsubsection.153}
\contentsline {section}{\numberline {4.2}Implementation}{44}{section.164}
\contentsline {subsection}{\numberline {4.2.1}Brand Data Collection}{44}{subsection.165}
\contentsline {subsection}{\numberline {4.2.2}Training Set}{47}{subsection.232}
\contentsline {subsection}{\numberline {4.2.3}Twitter Project}{48}{subsection.240}
\contentsline {subsubsection}{\numberline {4.2.3.1}Branty.java}{49}{subsubsection.241}
\contentsline {subsubsection}{\numberline {4.2.3.2}FrequentWordsFeature.java}{50}{subsubsection.242}
\contentsline {subsubsection}{\numberline {4.2.3.3}KloutAPI.java}{50}{subsubsection.243}
\contentsline {subsubsection}{\numberline {4.2.3.4}MLAnnotator.java}{51}{subsubsection.246}
\contentsline {subsubsection}{\numberline {4.2.3.5}MongoDB.java}{51}{subsubsection.247}
\contentsline {subsubsection}{\numberline {4.2.3.6}SWN3.java}{52}{subsubsection.248}
\contentsline {subsubsection}{\numberline {4.2.3.7}WEKA.java}{52}{subsubsection.253}
\contentsline {subsection}{\numberline {4.2.4}Feature Extraction Project}{53}{subsection.259}
\contentsline {subsection}{\numberline {4.2.5}Web Application}{55}{subsection.262}
\contentsline {subsubsection}{\numberline {4.2.5.1}Front end}{55}{subsubsection.263}
\contentsline {subsubsection}{\numberline {4.2.5.2}Back-end}{65}{subsubsection.374}
\setcounter {tocdepth}{2}
\contentsline {chapter}{\numberline {5}\MakeUppercase {Results}}{71}{chapter.381}
\contentsline {section}{\numberline {5.1}Data Set}{71}{section.382}
\contentsline {section}{\numberline {5.2}Regression Analysis}{71}{section.383}
\setcounter {tocdepth}{2}
\contentsline {chapter}{\numberline {6}\MakeUppercase {Review of Literate}}{72}{chapter.384}
\setcounter {tocdepth}{2}
\contentsline {chapter}{\numberline {7}\MakeUppercase {Discussion}}{73}{chapter.385}
\setcounter {tocdepth}{2}
\contentsline {chapter}{\numberline {8}\MakeUppercase {Limitations and Future Work}}{74}{chapter.386}
\contentsline {section}{\numberline {8.1}MongoDB Limitations}{74}{section.387}
\contentsline {section}{\numberline {8.2}Twitter API limitations}{74}{section.388}
\setcounter {tocdepth}{0}
\contentsline {chapter}{Appendix \numberline {A} --- \scshape {Samples of Program's code}}{76}{appendix.389}
\contentsline {section}{\numberline {A.1}Brand document}{76}{section.390}
\contentsline {section}{\numberline {A.2}Feature document}{77}{section.433}
\contentsline {section}{\numberline {A.3}Configurations documents}{78}{section.531}
\orig@contentsline {part}{APPENDICES}{}
\setcounter {tocdepth}{0}
\contentsline {chapter}{\MakeUppercase {References}}{80}{section*.605}
\setcounter {tocdepth}{0}
\setcounter {tocdepth}{3}
