 public static void parser(categotyName) {
        try {
            doc = Jsoup.connect("http://somesite.com").get();
            list = doc.select("div");
            for (int i = 0; i < list.size(); i++) {
		//selects a brand name
                brand = list.get(i).select("p.title"); 
		//selects a url with Twitter profile
                profile = list.get(i).select("a"); 
		//selects an image 
                photo = list.get(i).select("img"); 

                if (profile == null || profile.text().toString().trim().equals("")) {
                    br = brand.first().text();
                    prof = "";
                    pho = photo.first().absUrl("src");
                } else {
                    br = brand.first().text();
                    pho = photo.first().absUrl("src");
                    String followUrl = profile.first().attr("href");
                    prof = "@" + followUrl.substring(followUrl.lastIndexOf("=") + 1);

                }
		//stores in MongoDB the brand 
                BasicDBObject in = new BasicDBObject("name", br).
                        append("imgurl", pho).
                        append("profile", prof).
                        append("category", categotyName);
                db.insert(in, "Branty");

            }
        } catch (IOException ex) {
            System.out.println("An error must have occured");
            Logger.getLogger(Branty.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
