 $(document).on('click', '#features #brands li', function() {
            var imgsrc = $(this).find('img').attr("src");
            var followers = [];
            var lastKey;
            var features;
            var retweets = [];
            var rates = [];
            var position = [];
            var profile;
            $.ajax({
                type: 'GET',
                url: '../controllers/brandAjax.php',
                data: {name: brandName},
                dataType: 'json',
                async: false,
                success: function(data) {
                    $.each(data.features, function(key, value) {
                        followers.push([Date.parse(value.date), value.followers]);
                        retweets.push([Date.parse(value.date), value.retweet_count]);
                        lastKey = value.date;
                    });
                     $.each(data.rates, function(key, value) {
                        rates.push([value.date*1000, value.rate]);
                        position.push([value.date*1000, value.position]);
                    });
                    features = data.features[lastKey];
                    profile = data.profile[0];
                    Features.HighChart('.canvasCont #followersTrend','Followers', followers, "", "", false);
                    Features.HighChart('.canvasCont #ratesTrend', 'Rate', rates, "Position", position, true);
                },
                error: function() {
                    $('.proj-nav .container').append('<div id="error">Oops! Something went wrong...</div>');
                }
