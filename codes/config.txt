/* 0 */
{
    "_id" : "config_features",
    "positive" : "max",
    "negative" : "min",
    "positive_trending" : "max",
    "tweet_count" : "max",
    "URL_mention_count" : "max",
    "mention_count" : "max",
    "klout_score" : "max",
    "listed" : "max",
    "followers" : "max",
    "tweets_per_month" : "max",
    "verified" : "max",
    "last_week_tweet_count" : "max",
    "retweet_count" : "max",
    "favorite_count" : "max",
    "followers_div_friends" : "max"
}

/* 1 */
{
    "_id" : "config_trends",
    "Technology" : [ 
        "android", 
        "gameinsight", 
        "androidgames", 
        "tech", 
        "job", 
        "apple", 
        "technology", 
        "iphone", 
        "gadgets", 
        "wordpress"
    ],
    "Fashion" : [ 
        "kofapp", 
        "auction", 
        "fashion", 
        "peawards", 
        "shoes", 
        "freekicks", 
        "golf", 
        "style", 
        "sneakers", 
        "giveaway"
    ],
    "Auto" : [ 
        "cars", 
        "usedcars", 
        "trucks", 
        "csrracing", 
        "giveaway", 
        "raceyourfriends", 
        "tech", 
        "autotrade", 
        "buycar", 
        "buyusedcar"
    ],
    "Food/Beverages" : [ 
        "giveaway", 
        "giveaway", 
        "coffee", 
        "food", 
        "glenview", 
        "foodporn", 
        "yum", 
        "sales", 
        "crowdtappers", 
        "marketing"
    ]
}