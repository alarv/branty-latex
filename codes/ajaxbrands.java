$.ajax({ 
	type: 'GET', 
	url: '../controllers/topsis.php', 
	data: Main.weights, 
	dataType: 'json', 
	async: true, 
	success: function(data) { 
		//Responsible for displaying the results 
		Features.DisplayBrands(data); 
	}, 
	error: function() { 
		$('.proj-nav .container').append('Oops! Something went wrong...'); 
	}
 }); 
