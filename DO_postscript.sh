#!/bin/sh -e

# initial latex
latex branty.tex

# make bibliography
bibtex branty

# make glossary
bibtex branty.gls

# make list of symbols 
bibtex branty.losa

# make index
makeindex -s gatech-thesis-index.ist branty.idx

# twice more
latex branty.tex
latex branty.tex

# make postscript
dvips -G0 -Ppdf branty

